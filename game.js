function GameOfLife(){
  this.width=12;
  this.height=12;
  this.interval=null
  this.cells=null
}
GameOfLife.prototype.createTable=function(){
var lifeScenario=document.createElement('tbody')
var rows=''
for (let h = 0; h < this.height; h++) {
  rows+=`<tr id='${h}'>`
  for (let w = 0; w < this.width; w++) {
    rows+=`<td class='dead' id='${h}-${w}'></td>`
  }
  rows+='</tr>'
}
lifeScenario.innerHTML=rows
return lifeScenario
}
//-----------------------------------------------------------------------------------------------------------------------
GameOfLife.prototype.getCells=function(){
  this.cells=document.querySelectorAll('td')
}
//-----------------------------------------------------------------------------------------------------------------------
GameOfLife.prototype.setClickMethod=function(){
  if (this.cells){
    //if cells exists
   for (let i = 0; i < this.cells.length; i++) {
    this.cells[i].onclick=function(){
      this.className=this.className=='dead'?'alive':'dead'
    } //onclick
   }//for
  }//if
}
//-----------------------------------------------------------------------------------------------------------------------
GameOfLife.prototype.step=function(){
  var list=[]
  for (let h = 0; h < this.height; h++) {
    for (let w = 0; w < this.width; w++) {
      this.check(`${h}-${w}`) && list.push(`${h}-${w}`)
    }
  }
  for (let i = 0; i < this.cells.length; i++) {
    this.cells[i].className=list.indexOf(this.cells[i].id)==-1?'dead':'alive'
   }//for
}
//-----------------------------------------------------------------------------------------------------------------------
GameOfLife.prototype.alivesCells=function(id){
  //return the number of alive cells
  row=Number(id.split('-')[0])
  col=Number(id.split('-')[1])
  list=[[row-1,col-1],[row,col-1],[row+1,col-1],[row-1,col],[row+1,col],[row-1,col+1],[row,col+1],[row+1,col+1]]
  var alives=0
  for (let i = 0; i < list.length; i++) {
    if (list[i][0]>=0 && list[i][1]>=0 && list[i][0]<this.height && list[i][1]<this.width){
      alives+=document.getElementById(`${list[i][0]}-${list[i][1]}`).className=='alive'?1:0
    }
  } return alives
}
//-----------------------------------------------------------------------------------------------------------------------
GameOfLife.prototype.check=function(id){
  //false if the cell must be dead, true if it must be alive
var cellsAlives=this.alivesCells(id)
var status=document.getElementById(id).className
if(status=='dead' && cellsAlives==3) return true
if(status=='alive' && cellsAlives>=2 && cellsAlives<=3) return true
return false
}
//-----------------------------------------------------------------------------------------------------------------------
GameOfLife.prototype.pause=function(){
  if(this.interval){
    clearInterval(this.interval)
    this.interval=null
  }
}
GameOfLife.prototype.random=function(){
if(!this.interval){
  for (let i = 0; i < this.cells.length; i++) {
    this.cells[i].className=Math.floor(Math.random() * (10 - 1))%2==0?'dead':'alive'
   }
}
}
GameOfLife.prototype.clearBoard=function(){
  if(!this.interval){
    for (let i = 0; i < this.cells.length; i++) {
      this.cells[i].className='dead'
     }
  }
  }

GameOfLife.prototype.play=function(){
  if(!this.interval){
    this.interval=setInterval(this.step.bind(this),1000)
  }
}

  var game=new GameOfLife()
  //create the table
  document.getElementById('board').appendChild(game.createTable())
  //set cells
  game.getCells()
  //set cells methods
  game.setClickMethod()
  //
  document.getElementById('step_btn').onclick=game.step.bind(game)
  
  document.getElementById('play_btn').onclick=game.play.bind(game)
  document.getElementById('reset_btn').onclick=game.random.bind(game)
  document.getElementById('clear_btn').onclick=function(){
    game.pause()
    game.clearBoard()
  }

