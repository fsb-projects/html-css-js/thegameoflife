# Game Of Life

## Overview

### ¿Qué es Game of Life?

[Conway's Game of Life](https://en.wikipedia.org/wiki/Conway's_Game_of_Life) es un set de reglas governando la destrucción, persistencia o propagación de celdas vecinas en un grid - una pseudo-simulación de vida. Fue creada por John Horton Conway en 1970, en un esfuerzo de simplificar un concepto por el matematico John von Neumann en los '40s. El proposito y el poder del juego no es simular realisticamente la vida, pero en servir como un simple sistema que produce comportamiento complejo. De hecho, el Game of Life es una [maquina de Turing universal](https://en.wikipedia.org/wiki/Turing_machine), capaz de modelar cualquier calculación algoritmica.

Aquí hay un [video de ejemplo](https://www.youtube.com/watch?v=C2vgICfQawE) demostrando varios de los patrones complejos que Game of Life puede producir. 

## START
Abre el Index HTML y comienza a jugar

[Live Preview](https://fsb-gameoflife.netlify.com/)